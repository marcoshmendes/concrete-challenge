var jwt = require('jsonwebtoken');
var config = require('../../config/jwt');

module.exports = {
	getToken: function(user) {
    	return jwt.sign({
	        	user_id: user._id
	      	},
	      	config.jwtSettings.secret,
	      	{
	        	algorithm: config.jwtSettings.algorithm,
	        	expiresIn: config.jwtSettings.expires,
	      	}
    	);
  	},

  	verifyToken: function(token, verified) {
  		return jwt.verify(token, process.env.tokenSecret || "6UdsiudsjkH3898jLDLkmcdww8djdsi389843oiUSnZ9zxooo8w", {}, verified);
  	}
}