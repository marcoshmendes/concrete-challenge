var bcrypt = require('bcrypt-nodejs');

module.exports = {
	hashPassword: function (password) {
	  if (password) {
	  		return bcrypt.hashSync(password);
	  }
	},
	 
	comparePassword: function(password, user){
	  return bcrypt.compareSync(password, user.password);
	}
}