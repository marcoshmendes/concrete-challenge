var User = require('mongoose').model('User');
var jwt = require('jsonwebtoken');
var encrypt = require('../services/encrypt');
var token = require('../services/token');

exports.signup = function(req, res) {
	var user = new User(req.body);

	User.count({
		email: user.email
	}, function (err, count){ 
		if (err) {
			res.status(500).send({
				message: 'Error on create user'
			});
			return;
		}

	    if (count) {
	        res.status(409).send({
				message: 'This email already exist'
			});
	    } else {
	    	var now = new Date();

	    	user.token = token.getToken(user);
	    	user.created_at = now;
	    	user.updated_at = now;
	    	user.last_login = now;
	    	user.password = encrypt.hashPassword(user.password);

		    User.create(user, function(err, user) {
				if (err) {
					res.status(500).send({
						message: 'Error on create user'
					});
					return;
				}
				res.status(200).send(user);
			});
	    }
	}); 
}

exports.signin = function(req, res) {
	var credentials = req.body;

	User.findOne({
		email: credentials.email
	}, function(err, user) {
		if (err) {
			res.status(500).send({
				message: 'Error on sign in'
			});
		}

		if (user) {
			var matchPassword = encrypt.comparePassword(credentials.password, user);
			var now = new Date();

			if (!matchPassword) {
				res.status(401).send({
					message: 'invalid username or password'
				});
				return;
			}

			user.last_login = now;
			user.updated_at = now;
			user.save(user, function(err, updatedUser) {
				if (err) {
					res.status(500).send({
						message: 'Error on signin'
					});
				}
				res.status(200).send(updatedUser);
			});
		} else {
			res.status(404).send({
				message: 'invalid username or password'
			});
		}
	});
}

exports.search = function(req, res) {
	var token = req.body.token || req.query.token || req.headers['authorization'];
	var user_id = req.query.user_id;
	
	if (!token) {
		res.status(401).send({
			message: 'Unauthorized'
		});
		return;
	}

	if (user_id) {
		User.findOne({
			_id: user_id
		}, function(err, user) {
			if (err) {
				res.status(500).send({
					message: 'Error on find user'
				});
			}

			if (user && user.token) {
				var regex = /Bearer/;

				if (regex.test(token)) {
					token = token.replace('Bearer ', '');
				}

				if (user.token == token) {
					var now = new Date();
					var diff = now.getTime() - new Date(user.last_login).getTime();
					var minutes = Math.round(diff / 60000);

					if (minutes > 30) {
						res.status(403).send({
							message: 'Invalid Session'
						});
						return;
					}

					res.status(200).send(user);
				} else {
					res.status(401).send({
						message: 'Unauthorized'
					});
				}
			} else {
				res.status(400).send({
					message: 'user_id not exist'
				});
			}
			
		});
	} else {
		res.status(400).send({
			message: 'missing user_id'
		});
	}



}