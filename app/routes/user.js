var user = require('../../app/controllers/user');

module.exports = function(app) {
	app.route('/signup').post(user.signup);
	app.route('/signin').post(user.signin);
	app.route('/search').get(user.search);
	app.use(function(req, res, next) {
	    if (!req.route) {
			res.status(404).send({message: 'endpoint not found'});  
			next();
	    }
	});
};
