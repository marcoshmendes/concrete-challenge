var mongoose = require('mongoose');
var encrypt = require('../services/encrypt');
var Schema = mongoose.Schema;
var UserSchema;

var Phone = new Schema({
  number: String,
  ddd: Number
});

UserSchema = new Schema({
	name: String,
	email: String,
	phones: [Phone],
	password: String,
	token: String,
	created_at: Date,
	updated_at: Date,
	last_login: Date
});

mongoose.model('User', UserSchema);