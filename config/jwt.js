var expires = 60 * 24;
var secret = process.env.tokenSecret || "6UdsiudsjkH3898jLDLkmcdww8djdsi389843oiUSnZ9zxooo8w";
var algorithm = "HS256";


module.exports.jwtSettings = {
  expires: expires,
  secret: secret,
  algorithm : algorithm
};