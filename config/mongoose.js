var config = require('./config');
var mongoose = require('mongoose');

module.exports = function() {
	var db;

	db = mongoose.connect(config.db, { useMongoClient: true });
	mongoose.Promise = global.Promise;

	require('../app/models/user');
	return db;
};