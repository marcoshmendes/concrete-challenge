process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var mongoose = require('./config/mongoose');
var express = require('./config/express');
var config = require('./config/config');

var db = mongoose();
var app = express();

app.listen(config.listen_port);

module.exports = app;
console.log('API ready http://localhost:' + config.listen_port + '/');