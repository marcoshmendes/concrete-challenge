# Concrete Challenge - Node.js
### API RESTful sign in/ sign up
##### A MEAN structure using CommonJS Pattern

Endpoints:


* http://desafio.marcoshmendes.com/signup (POST)

* http://desafio.marcoshmendes.com/signin (POST)

* http://desafio.marcoshmendes.com/search?user_id=5a125e2ee6650b2eb74eb16b (GET)


MongoDB URI connection string:
```
mongodb://concrete:Sy#90dK@concrete-shard-00-00-eaaam.mongodb.net:27017,concrete-shard-00-01-eaaam.mongodb.net:27017,concrete-shard-00-02-eaaam.mongodb.net:27017/test?ssl=true&replicaSet=concrete-shard-0&authSource=admin
```

Hosted on: **DigitalOcean**

